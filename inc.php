<?php

/**
 * Главный подключаемый файл ядра системы
 * 
 * @author Eugeny Leonov <eleonov@leolab.info>
 * @copyright 2020 "Лаборатория Леонова" (http://leolab.info/)
 * 
 * @package SWC7
 * @subpackage core
 * 
 * @version 0.1.0
 * @see http://swc.leolab.info/docs/f/core/inc.php
 */

namespace swc;

if (defined('swc_base')) {
	die('Duplicate call<pre>' . "\n" . print_r(debug_backtrace(false), 1) . "\n" . '</pre>');
}
// Установка времени запуска (подключения) ядра системы
$GLOBALS['swc._start']['time'] = microtime(true);
// Регистрация обработчиков
\spl_autoload_register('\swc\_autoload', true);
\register_shutdown_function('\swc\_shutdown');
\set_exception_handler('\swc\exceptionHandler');

//== Установка констант системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
define('\swc\swc_base', dirname(__FILE__));
if (!defined('\swc\base_path')) {
	if (isset($_SERVER['swc_basePath']) && is_string($_SERVER['swc_basePath'])) {
		define('\swc\base_path', $_SERVER['swc_basePath']);
	} else {
		define('\swc\base_path', $_SERVER['DOCUMENT_ROOT']);
	}
}
if (!defined('\swc\data_path')) {
	if (isset($_SERVER['swc_dataPath']) && is_string($_SERVER['swc_dataPath'])) {
		//@todo: Обработка относительного пути
		define('\swc\data_path', $_SERVER['swc_dataPath']);
	} else {
		define('\swc\data_path', \swc\base_path . '/data');
	}
}
if (!defined('mdl_folder')) {
	if (isset($_SERVER['swc_mdlFolder']) && is_string($_SERVER['swc_mdlFolder'])) {
		define('\swc\mdl_folder', $_SERVER['swc_mdlFolder']);
	} else {
		define('\swc\mdl_folder', 'mdl');
	}
}
if (!defined('\swc\dbg_enabled')) {
	if (isset($_SERVER['swc_dbgEnabled'])) {
		define('\swc\dbg_enabled', true);
	} else {
		define('\swc\dbg_enabled', false);
	}
}
if (!defined('\swc\dbg_file')) {
	if (isset($_SERVER['swc_dbgFile']) && is_string($_SERVER['swc_dbgFile'])) {
		define('\swc\dbg_file', $_SERVER['swc_dbgFile']);
	}
}
if (!defined('\swc\err_file')) {
	if (isset($_SERVER['swc_errFile']) && is_string($_SERVER['swc_errFile'])) {
		define('\swc\err_file', $_SERVER['swc_errFile']);
	}
}
if (!defined('\swc\log_file')) {
	if (isset($_SERVER['swc_logFile']) && is_string($_SERVER['swc_logFile'])) {
		define('\swc\log_file', $_SERVER['swc_logFile']);
	}
}
//<< Установка констант системы ===============================================

//== Инициализация сессии >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
switch (session_status()) {
	case PHP_SESSION_DISABLED:
		throw new ESWC_SessionDisabled();
		break;
	case PHP_SESSION_ACTIVE:
		// Сессии уже инициализированы
		break;
	case PHP_SESSION_NONE:
		// (?) Используем свои сессии?
		if (!session_start()) {
			die('Can not start session');
		}
		break;
}
// Буфер вывода открывать надо не здесь.
if (!ob_start()) {
	die('Can not start buffer');
}
//<< Инициализация сессии =====================================================


//== Обработчики >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/**
 * Автолоадер классов системы
 * 
 * @version 0.1.0 (03/04/2020)
 * @see http://swc.leolab.info/docs/fn/_autoload
 */
function _autoload(string $cName)
{
	$cl = false;
	$sa = [];
	if (\strpos($cName, '\\') !== false) {
		$ca = \explode('\\', $cName);
		if (\substr($ca[\count($ca) - 1]) == 'ESWC') {
			$c = \array_pop($ca);
			$cp = \implode('/', $ca);
			$sa[] = \swc\base_path . '/' . \swc\mdl_folder . '/' . $cp . '/E/' . $c;
			$sa[] = \swc\swc_base . '/mdl/' . $cp . '/E/' . $c;
		} else {
			$cp = implode('/', $ca);
			$sa[] = \swc\base_path . '/' . \swc\mdl_folder . '/' . $cp . '/class.php';
			$sa[] = \swc\base_path . '/' . \swc\mdl_folder . '/' . $cp . '.class';
			$sa[] = \swc\swc_base . '/mdl/' . $cp . '/class.php';
			$sa[] = \swc\swc_base . '/mdl/' . $cp . '.class.php';
		}
	} else {
		if (substr($cName, 0, 4) == 'ESWC') {
			$sa[] = \swc\base_path . '/' . \swc\mdl_folder . '/E/' . $cName;
			$sa[] = \swc\swc_base . '/E/' . $cName;
		} else {
			$sa[] = \swc\base_path . '/' . \swc\mdl_folder . '/' . $cName . '/class';
			$sa[] = \swc\base_path . '/' . \swc\mdl_folder . '/' . $cName . '.class';
			$sa[] = \swc\swc_base . '/mdl/' . $cName . '/class';
			$sa[] = \swc\swc_base . '/mdl/' . $cName . '.class';
			$sa[] = \swc\swc_base . '/' . $cName . '.class';
		}
	}
	foreach ($sa as $k => $v) {
		if (\file_exists($v . '.php')) {
			include_once($v . '.php');
			if (\method_exists($cName, '__init') && \is_callable([$cName, '__init'])) {
				\call_user_func([$cName, '__init']);
			}
			return;
		}
	}
	\swc\_e('[E] Class not found (' . $cName . ')', \debug_backtrace());
	return (false);
}

/**
 * Обработчик исключений
 * 
 * @version 0.1.0 (03/04/2020)
 * @see http://swc.leolab.info/docs/fn/exceptionHandler
 */
function exceptionHandler($e)
{
	while (\ob_get_level() > 0) {
		\ob_end_clean();
	}
	$f = '';
	$l = 0;
	if (\headers_sent($f, $l)) {
		echo ("\n<p>\nUnexpected output started at $f:$l\n</p>\n");
	} else {
		\http_response_code(500);
		\header('content-type: text/plain; charset=utf-8', true);
	}
	\swc\_e('Exception: ' . $e, $e);
	die($e);
}

/**
 * Обработчик завершения работы
 * 
 * @version 0.1.0 (03/04/2020)
 * @see http://swc.leolab.info/docs/fn/_shutdown
 */
function _shutdown()
{
	$err = false;
	$sHead = 'Request(' . \session_id() . ') (' . $_SERVER['REQUEST_URI'] . ') from (' . $_SERVER['REMOTE_ADDR'] . ') finished. Time: ' . (\microtime(true) - $GLOBALS['swc._start']['time']) . 'sec., Memory total: ' . \memory_get_peak_usage(true) . ', used: ' . \memory_get_peak_usage();
	if (isset($_GLOBALS['swc._dbg']) && \is_array($_GLOBALS['swc._dbg'])) {
		if (defined('\swc\dbg_file') && (\swc\dbg_file != '')) {
			$s = [$sHead,];
			foreach ($_GLOBALS['swc._dbg'] as $va) {
				$p = '';
				foreach ($va as $v) {
					$s[] = $p . '[' . $v['from'] . '] ' . $v['msg'];
					if (!is_null($v['data'])) {
						$s[] = \print_r($v['data'], true);
					}
					//$p=$p."\t";
				}
			}
			\file_put_contents(\swc\dbg_file, \implode("\n", $s), \FILE_APPEND | \LOCK_EX);
		}
	}
	if (isset($_GLOBALS['swc._err']) && \is_array($_GLOBALS['swc._err'])) {
		if (defined('\swc\err_file') && (\swc\err_file != '')) {
			$s = [$sHead,];
			foreach ($_GLOBALS['swc._err'] as $va) {
				$p = '';
				foreach ($va as $v) {
					$s[] = $p . '[' . $f['from'] . '] ' . $v['msg'];
					if (!is_null($v['data'])) {
						$s[] = \print_r($v['data'], true);
					}
					//$p=$p."\t";
				}
			}
			\file_put_contents(swc\err_file, \implode("\n", $s), \FILE_APPEND | \LOCK_EX);
		}
	}
	if (\defined('\swc\log_file')) {
		// Запись лога обращений
		\file_put_contents(swc\log_file, $sHead, \FILE_APPEND | \LOCK_EX);
	}
}

//<< Обработчики ==============================================================

//== Вспомогательные функции >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/**
 * Сообщение отладки
 * 
 * @version 0.1.0 (03/04/2020)
 * 
 * @param string $msg
 * @param null|array|object $data
 * @param null|string $di
 */
function _d(string $msg, $data = null, $di = null): ?string
{
	if (defined('swc\dbg_enabled') && (\swc\swc_dbg == true)) {
		if (\is_null($di)) {
			$di = \uniqid('', true);
		}
		$GLOBALS['swc._dbg'][$di][] = [
			'from' => \swc\_from(),
			'msg' => $msg,
			'data' => $data,
		];
		return ($di);
	}
	return null;
}

/**
 * Сообщение ошибки
 * 
 * @version 0.1.0 (03/04/2020)
 * 
 * @param string $msg
 * 
 */
function _e(string $msg, $data = null, $di = null): ?string
{
	$di = _d('[ERROR] ' . $msg, $data, $di);
	$GLOBALS['swc._err'][$di][] = [
		'from' => _from(),
		'msg' => $msg,
		'data' => $data,
	];
	return ($di);
}

/**
 * Откуда вызвано
 * 
 * @version 0.1.0 (20/03/2020)
 * 
 * @param int $lvl=1
 * @return string
 */
function _from(int $lvl = 1): string
{
	$d = debug_print_backtrace(false, $lvl + 1);
	if (!isset($d[$lvl]['file'])) {
		_d('_from(' . $lvl . ')', debug_backtrace(false));
		return ('Error: stack level(' . $lvl . ') not exists, required in: ' . $d[0]['file'] . ':' . $d[0]['line']);
	}
	return ($d[$lvl]['file'] . ':' . $d[$lvl]['line']);
}
//<< Вспомогательные функции ==================================================
