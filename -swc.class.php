<?php
/**
 * Основной класс системы
 * 
 * @author Eugeny Leonov <eleonov@leolab.info>
 * @copyright 2020 "Лаборатория Леонова" (http://leolab.info/)
 * 
 * @version 0.1.0 (03/04/2020)
 */

if(!defined('swc_base')){die('Incorrect class call');}

class swc{

	private static $_vars=[];
	private static $_instance=null;

	/**
	 * Получить экземпляр класса
	 *
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return \swc
	 */
	public static function getInstance():\swc{
		if(static::$_instance==null){
			static::$_instance=new static();
			static::$_instance->__init();
		}
		return(static::$_instance);
	}

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	/**
	 * Инициализация класса
	 *
	 * @version 0.1.0 (03/04/2020)
	 * 
	 * @return void
	 */
	protected function __init(){
		static::$_vars['req']=new \swc\req();
	}

	/**
	 * Объединяющий метод
	 *
	 * @version 0.1.0 (03/04/2020)
	 * 
	 * @return void
	 */
	public static function run(){
		$s=self::getInstance();
		$s->start();
		$s->prepare();
		$s->parse();
		$s->execute();
		$s->render();
		$s->send();
		$s->finish();
	}

	/**
	 * Старт обработки
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	public function start(){
		static::$_vars['cfg']=new \swc\cfg('core',['shared'=>true,'readonly'=>true]);
		static::$_vars['rsp']=$this->__getRsp();
	}

	/**
	 * Подготовка к выполнению
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	public function prepare(){
		if(!isset($this->rsp)){\swc\ESWC_Error('ESWC_CoreNotInitialized');}
		$this->rsp->prepare();
	}

	/**
	 * Обработать данные
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	public function parse(){
		if(!isset($this->rsp)){\swc\ESWC_Error('ESWC_CoreNotInitialized');}
		$this->rsp->parse();
	}

	/**
	 * Выполнить действия
	 *
	 * @version 0.1.0 (03/04/2020)
	 * 
	 * @return void
	 */
	public function execute(){
		if(!isset($this->rsp)){\swc\ESWC_Error('ESWC_CoreNotInitialized');}
		$this->rsp->execute();
	}

	/**
	 * Сформировать ответ
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	public function render(){
		if(!isset($this->rsp)){\swc\ESWC_Error('ESWC_CoreNotInitialized');}
		$this->rsp->render();
	}

	/**
	 * Отправить данные
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	public function send(){
		if(!isset($this->rsp)){\swc\ESWC_Error('ESWC_CoreNotInitialized');}
		$this->rsp->send();
	}

	/**
	 * Завершение работы
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	public function finish(){}

	/**
	 * Получить экземпляр класса ответа
	 * 
	 * @version 0.1.0 (03/04/2020)
	 *
	 * @return void
	 */
	private function __getRsp(){
		if(isset($this->req['act'])){return new \swc\act($this->req);}
		if(isset($this->req['get'])){return new \swc\get($this->req);}

		return new \swc\page($this->req);
	}


	public function __set($key,$val){
		switch($key){
			case 'req':throw new \swc\ESWC_PropertyReadOnly($this,$key);break;
			//case 'cfg':throw new \swc\ESWC_PropertyReadOnly($this,$key);break;
			case 'cfg':if(!is_a($val,'\swc\cfg')){throw new \swc\ESWC_InvalidPropertyType($val,'\swc\cfg','req');}else{static::$_vars['cfg']=$val;}break;
			case 'usr':if(!is_a($val,'\swc\usr')){throw new \swc\ESWC_InvalidPropertyType($val,'\swc\usr','usr');}else{static::$_vars['usr']=$val;}break;
			case 'rsp':if(!is_a($val,'\swc\rsp')){throw new \swc\ESWC_InvalidPropertyType($val,'\swc\rsp','rsp');}else{static::$_vars['rsp']=$val;}break;
			case 'dbi':if(!is_a($val,'\swc\dbi')){throw new \swc\ESWC_InvalidPropertyType($val,'\swc\dbi','dbi');}else{static::$_vars['dbi']=$val;}break;
			default:
				throw new \swc\ESWC_UndefinedProperty($this,$key);
		}
	}

	public function __unset($key){
		throw new \swc\ESWC_ReadOnlyProperty($this,$key);
	}

	public function __get($key){}
}