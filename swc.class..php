<?php

/**
 * Класс ядра системы
 *
 * @version 0.1.0 (14/05/2020)
 */

class swc
{

	static private $instance = null;

	static private $classAliases = [];
	static private $classMap = [];
	static private $classes = [];
	static private $funcFiles = [];

	static public $conf = []; // Конфигурация системы

	/**
	 * Singleton
	 *
	 * @return \swc
	 */
	static public function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return (self::$instance);
	}

	/**
	 * Конструктор класса
	 */
	private function __construct()
	{
		self::__init();
		spl_autoload_register('\swc::classLoad', true, true);
		register_shutdown_function('\swc::_shutdown');
		//== Инициализация классов ядра >>
		self::$classes['req'] = self::getClass('req');
		if (isset(self::$conf['dbi'])) {
			self::$classes['dbi'] = self::getClass('dbi', self::$conf['dbi']);
		}
		//<< Инициализация классов ядра ==
	}

	/**
	 * Инициализация класса
	 */
	static private function __init()
	{
		if (!class_exists('\swc\cf')) {
			require_once(__DIR__ . '/cf.class.php');
		}
		self::__init_loadConf();
	}

	/**
	 * Загрузка конфигурации
	 */
	static private function __init_loadConf()
	{
		self::__init_loadConf_loadClassAliases();
		self::__init_loadConf_loadClassCache();
		if (defined('swc_site')) {
			if (\swc\cf::exists(base_path . '/sites/' . swc_site . '/.conf/core')) {
				self::$conf = \swc\cf::load(base_path . '/sites/' . swc_site . '/.conf/core');
			}
		}
	}

	/**
	 * Загрузка алиасов классов
	 */
	static private function __init_loadConf_loadClassAliases()
	{
		if (defined('swc_site')) {
			if (\swc\cf::exists(base_path . '/sites/' . swc_site . '/.conf/class_aliases')) {
				self::$classAliases = \swc\cf::load(base_path . '/sites/' . swc_site . '/.conf/class_aliases');
				return;
			}
		}
		if (\swc\cf::exists(base_path . '/.conf/class_aliases')) {
			self::$classAliases = \swc\cf::load(base_path . '/.conf/class_aliases');
			return;
		}
		if (\swc\cf::exists(swc_base . '/.conf/class_aliases')) {
			self::$classAliases = \swc\cf::load(swc_base . '/.conf/class_aliases');
		}
	}

	/**
	 * Загрузка кеша файлов классов
	 */
	static private function __loadClassCache()
	{
		if (defined('swc_site')) {
			if (\swc\cf::exists(base_path . '/sites/' . swc_site . '/.cache/classes')) {
				self::$classMap = \swc\cf::load(base_path . '/sites/' . swc_site . '/.cache/classes');
				return;
			}
		}
		if (\swc\cf::exists(base_path . '/.cache/classes')) {
			self::$classMap = \swc\cf::load(base_path . '/.cache/classes');
		}
	}

	/**
	 * Сохранение кеша файлов классов
	 */
	static private function __saveClassCache()
	{
		if (defined('swc_site')) {
			\swc\cf::save(base_path . '/sites/' . swc_site . '/.cache/classes', self::$classMap);
			return;
		}
		\swc\cf::save(base_path . '/.cache/classes', self::$classMap);
	}

	/**
	 * Проверка корректности имени файла
	 * 
	 * @param string
	 * @return string|null
	 */
	static public function FName(string $fName): ?string
	{
		if (strlen($fName) < 1) {
			ESWC_Error('invalidValue', swc::trans('swc.invalidValue', ['value' => $fName, 'expect' => 'correct file name']));
		}
		if (!ctype_alnum($fName) || !preg_match('/^(?:[a-z0-9_-]|\.(?!\.))+$/iD', $fName)) {
			return ESWC_Error('invalidValue', swc::trans('swc.invalidValue', ['value' => $fName, 'expect' => 'correct file name']));
		}
		return base_path . '/' . $fName;
	}

	//== Обработчики >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	/**
	 * Завершение работы
	 */
	static public function _shutdown()
	{
		file_put_contents(base_path . '/access.log', 'req: ' . $_SERVER['REQUEST_URI'] . '?' . $_SERVER['QUERY_STRING'] . "\n", LOCK_EX | FILE_APPEND);
	}
	//<< Обработчики ==========================================================

	//== Загрузка классов >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * Получить экземпляр класса
	 */
	static public function getClass(string $cName, $opts = null)
	{
		return new $cName($opts);
	}

	/**
	 * Class autoloader
	 *
	 * @param string $class
	 * @return void
	 */
	static public function classLoad(string $class)
	{
		$realClass = $class;
		if (is_array(self::$classAliases) && isset(self::$classAliases[$class])) {
			$realClass = self::$classAliases[$class];
		}
		$classFile = self::findClassFile($realClass);

		if (is_array(self::$classMap)) {
			if (isset(self::$classMap[$realClass]) && file_exists(self::$classMap[$realClass] . '.php')) {
				$classFile = self::$classMap[$realClass];
			}
		} else {
			$classFile = self::findClassFile($realClass);
		}
		if (is_null($classFile)) {
			return (false);
		}
		require_once($classFile . '.php');
		if ($realClass != $class) {
			class_alias($class, $realClass, false);
		}
		if (method_exists($realClass, '_init')) {
			call_user_func($realClass . '::_init');
		}
		return (true);
	}

	static private function findClassFile(string $class): ?string
	{
		if (!is_array(self::$classMap) || !isset(self::$classMap[$class])) {
			if (strpos($class, '\\') !== false) {
				$ns = str_replace('\\', '/', substr($class, 0, strrpos($class, '\\')));
				$cName = substr($class, strrpos($class, '\\'));
			} else {
				$ns = null;
				$cName = $class;
			}
			$sa = [];
			if (defined('swc_site')) {
				if (!is_null($ns)) {
					$sa[] = base_path . '/sites/' . swc_site . '/mdl/' . $ns . '/' . $cName . '/class';
					$sa[] = base_path . '/sites/' . swc_site . '/mdl/' . $ns . '/' . $cName;
				} else {
					$sa[] = base_path . '/sites/' . swc_site . '/mdl/' . $cName . '/class';
					$sa[] = base_path . '/sites/' . swc_site . '/mdl/' . $cName;
				}
			}
			if (!is_null($ns)) {
				$sa[] = base_path . '/mdl/' . $ns . '/' . $cName . '/class';
				$sa[] = base_path . '/mdl/' . $ns . '/' . $cName;
				$sa[] = base_path . '/mdl/' . $ns . '/' . $cName . '/class';
				$sa[] = base_path . '/mdl/' . $ns . '/' . $cName;
			} else {
				$sa[] = base_path . '/mdl/' . $cName . '/class';
				$sa[] = base_path . '/mdl/' . $cName;
				$sa[] = base_path . '/mdl/' . $cName;
				$sa[] = base_path . '/mdl/' . $cName;
			}
			foreach ($sa as $f) {
				if (file_exists($f . '.php')) {
					$cFile = $f;
					break;
				}
			}
			if (is_null($cFile)) {
				return (null);
			}
			self::$classMap[$class] = $cFile;
			self::__save_classCache();
		}

		if (isset(self::$classMap[$class])) {
			return (self::$classMap[$class]);
		}
		return (null);

		if (!is_null($cFile)) {
			self::$classMap[$class] = $cFile;
			if (defined('swc_site')) {
				file_put_contents(base_path . '/sites/' . swc_site . '/swc_classMap.json', json_encode(self::$classMap), LOCK_EX);
			} else {
				file_put_contents(base_path . '/swc_classMap.json', json_encode(self::$classMap), LOCK_EX);
			}
		}
		return ($cFile);
	}
	//<< Загрузка классов =====================================================
	//== Instance methods >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public function run()
	{
		die('Run the core');
	}

	public function init()
	{
	}

	public function prepare()
	{
	}

	public function execute()
	{
	}

	public function parse()
	{
	}

	public function render()
	{
	}

	public function send()
	{
	}

	public function finish()
	{
	}

	//<< Instance methods =====================================================

	//== Virtual properties >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public function __get($name)
	{
		if (!isset(self::$classes[$name])) {
			self::$classes[$name] = $this->classLoad($name);
		}
		return (self::$classes[$name]);
	}

	public function __set($name, $value)
	{
	}

	public function __isset($name)
	{
		return (isset(self::$classes[$name]));
	}

	public function __unset($name)
	{
	}
	//<< Virtual properties ===================================================

	//== Virtual methods >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public function __call($name, $args)
	{
		if (function_exists($name)) {
			return call_user_func_array($name, $args);
		}
		if (!isset(self::$funcFiles[$name])) {
			if (!self::_findFunc($name)) {
				ESWC_Exception('FunctionNotFound', self::trans('swc.functionNotFound', ['name' => $name]), ['name' => $name, 'pars' => $args]);
			}
		}
		if (defined('swc_site')) {
			if (file_exists(base_path . '/sites/' . swc_site . '/fn/' . $name . '.php')) {
				return include(base_path . '/sites/' . swc_site . '/fn/' . $name . '.php');
			}
		}
		if (file_exists(base_path . '/fn/' . $name . '.php')) {
			return (include(base_path . '/fn/' . $name . '.php'));
		}
	}

	public function __callStatic($name, $args)
	{
	}
	//<< Virtual methods ======================================================
}

/**
 * Функция-обертка
 */
function swc()
{
	return (\swc::getInstance());
}

/**
 * Исключения системы
 */
function ESWC_Exception(string $eClass, string $msg, ?array $data = null)
{
	if (class_exists($eClass)) {
		throw new $eClass($msg, $data);
	}
	die('Exception: ' . $eClass . "<br>\n" . $msg . "<br>\n<pre>" . print_r($data, 1) . "</pre>\n");
	throw \ESWC_Exception($eClass, $msg, $data);
}

/**
 * Ошибки системы
 */
function ESWC_Error(string $eName, string $msg, ?array $data = null)
{
	ESWC_Exception($eName, $msg, $data);
}
