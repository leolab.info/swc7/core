<?php

/**
 * Класс неизвестного исключения
 */

class ESWC_UnknownException extends \Exception
{

    public function __construct($eClass, $msg, $data = null)
    {
    }
}
