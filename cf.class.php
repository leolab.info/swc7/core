<?php

/**
 * Класс работы с файлами данных
 */

namespace swc;

class cf
{

    /**
     * Существует ли файл данных
     */
    static public function exists(string $cName): bool
    {
        $fName = \swc::checkFName($cName);
        if (file_exists($fName . '.scf')) {
            return (true);
        }
        if (file_exists($fName . '.pcf')) {
            return (true);
        }
        if (file_exists($fName . '.jcf')) {
            return (true);
        }
    }

    /**
     * Загрузить данные файла
     */
    static public function load(string $cName): array
    {
        $fName = \swc::checkFName($cName);
        if (is_null($fName)) {
            return ([]);
        }
        if (file_exists($fName . '.scf')) {
            return (unserialize(file_get_contents($fName . '.scf')));
        }
        if (file_exists($fName . '.jcf')) {
            return (json_decode(file_get_contents($fName . '.jcf'), 1));
        }
        if (file_exists($fName . '.pcf')) {
            return (include($fName . '.pcf'));
        }
        return ([]);
    }

    /**
     * Сохранить данные в файл
     */
    static public function save(string $cName, array $data, string $type = 'scf'): bool
    {
        $fName = \swc::checkFName($cName);
        switch ($type) {
            case 'scf':
                return (file_put_contents($fName, serialize($data), LOCK_EX));
                break;
            case 'jcf':
                return (file_put_contents($fName, json_encode($data), LOCK_EX));
                break;
                /* featured, найти код
            case 'pcf':
            break;
            */
            default:
                \ESWC_Exception('ESWC_InvalidFileType', swc()->trans('cf.invalidFileType', ['type' => $type]));
        }
    }
}
