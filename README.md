# core

- [Документация](./docs/index.md)

С 14/05/2020 в очередной раз изменена парадигма системы.

Текущая:

Доступ к классу ядра, классам модулей ядра и всем методам осуществляется через функцию `swc()`:
```php
$reqFile=swc()->req->file;
$reqPath=swc()->req->path;
```

Доступ к статическим методам ядра осуществляется так же:
```php
$fname=swc::fname('/path/to/file');
```

Инициализация системы производится путем подключения файла `swc_root`/core.php, после чего появляется доступ к функции `swc()`

Выполнение запроса производится вызовом ```swc()->run()```

Код ядра и всех модулей вынесен из папки доступной из веба.
  - `.htaccess` - для веб-сервера Apache
  - `index.php` - основной обработчик запросов
  - `/res/` - папка в которой размещаются требуемые ресурсы (изображения, стили, скрипты). Папка создаётся автоматически, ресурсы размещаются подсистемой кеширования и компиляции.

Стартовый файл (%DOCUMENT_ROOT%/index.php) должен производить установку констант и подключение ядра системы.
```php
<?php
/* Рекомендуемый /index.php */
//define('site_root', '/srv/www/site.com'); // Константа site_root ядром системы не используется.
define('site_root', __DIR__); // Возможный вариант, но крайне не рекомендуемый
// Рекомендуется настраивать сервер таким образом, что бы не было доступа выше site_root
define('public_path', __DIR__); // Константа содержит путь к папке доступной из веба
define('base_path', site_root . '/app'); // В этой папке размещаются все дополнительные модули
define('swc_base', site_root . '/swc'); // В папке размещается ядро системы.
// Ядро системы может быть одно для нескольких разных сайтов. Но это требует менее безопасной конфигурации сервера.
define('swc_data', site_root . '/.data'); // Папка для хранения данных. В обязательном порядке доступ снаружи должен быть запрещен.

// Если приложение обрабатывает не корневые запросы, необходимо установить константу
//define('base_url', '/path/to/serve/');
// Если не используется mod_rewrite, константа устанавливается в:
//define('base_url','/index.php');
//define('swc_rewrite',false);
// Или, если сервер поддерживает PATH_INFO
//define('swc_rewrite','path');
// Если сервер нормально поддерживат mod_rewrite:
//define('base_url','/');
//define('swc_rewrite','rewrite');
// По умолчанию будет использоваться: swc_rewrite = false, base_url = $_SERVER['SCRIPT_FILENAME']

/* Вариант для обработки всего SWC */
include(swc_root . '/index.php');
die('Уже необязательно.');
```

```htaccess
## Рекомендуемый /.htaccess
Options +FollowSymLinks

php_flag short_open_tag on

SetEnv dbgEnabled 1

ErrorDocument 401 /index.php
ErrorDocument 403 /index.php
ErrorDocument 404 /index.php
<IfModule rewrite_module>
  SetEnv swc_rmMode rewrite
  RewriteEngine on
  RewriteCond %{REQUEST_URI} !^/res/*
  RewriteRule ^ index.php
</IfModule>
```

## Константы системы
- [base_path](#base_path) - полный путь к корню приложения.
- [swc_base](#swc_base) - полный путь к папке ядра.
- [swc_data](#swc_data) - имя папки для хранения данных
- [web_path](#web_path) - полный путь к папке обрабатываемой web-сервером
- [swc_site](#swc_site) - имя сайта, если установлено - поиск модулей, шаблонов и ресурсов производится в указанном сайте.
- [swc_theme](#swc_theme) - имя темы, если установлено - поиск шаблонов и ресурсов производится в указанной теме.
- [swc_log](#swc_log) - папка для записи логов. Если не установлено: [`base_path`/log/* || `base_path`/sites/`swc_site`/log/*]


### base_path
Корень приложения.

В идеале, к этой папке не должно быть доступа извне, так же как скрипты не должны иметь доступ выше указанной папки.

### swc_base
Папка ядра системы

В своей папке система хранит параметры по умолчанию.

Так же в неё производится установка обновлений ядра и базовых модулей.

### swc_data
Имя папки для хранения данных.

Папка располагается внутри `base_path`, полный путь к запрашиваемому файлу данных строится: `base_path`./.`swc_data`./path/to/file

Естественно, к этой папке доступа "снаружи" не должно быть.

### web_path
Корень публичной папки. По умолчанию определяется из `$_SERVER['DOCUMENT_ROOT']`

Единственная папка к которой должен быть доступ снаружи.

### swc_site
Имя текущего сайта.

Если не установлен - определяется из:
- конфигурации
- запроса `$_SERVER['HTTP_HOST']`
  - алиасы задаются конфигурацией [sites](./docs/conf.md#sites)

Определение папок текущего сайта (уточняется):
- данные `base_path`/sites/`swc_site`/`swc_data`
- модули и классы `base_path`/sites/`swc_site`/mdl/*
- шаблоны `base_path`/sites/`swc_site`/tpl/*
- ресурсы `base_path`/sites/`swc_site`/tpl/res/*
  - модулей `base_path`/sites/`swc_site`/mdl/`%mdl_name%`/tpl/res/*
  - шаблонов `base_path`/sites/`swc_site`/tpl/res/*

Темы являются общими для всех сайтов, ресурсы тем определяются:
- для модулей
- для шаблонов

"Кеширование" ресурсов (неточно):
- ресурсы сайта `web_path`/res/s/`swc_site`/path/file.ext
- ресурсы модулей `web_path`/res/m/`%mdl_name%`/path/file.ext
- ресурсы шаблонов `web_path`/res/t/%tpl_name%`/path/file.ext

## Установка
### git
```
$ cd %document_root%
$ git clone git@gitlab.com:leolab.info/swc7/core.git swc
```
В корне создаются файлы:
  - index.php
  - .htaccess

```php|index.php
<?php
define('app_root',__DIR__);
include(app_root.'/swc/core.php');
swc()->run();
?>
```

```|.htaccess
RewriteEngine on
RewriteCond %{REQUEST_URI} !^/res/*
RewriteRule ^ index.php
```

## История разработки
