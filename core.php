<?php

/**
 * Файл ядра системы
 *
 * @version 0.1.0 (14/05/2020)
 */

namespace swc;

if (!session_start()) {
	die('Can not start session');
}
// Сайт из сессии, надо перенести в swc::__construct()
if (isset($_SESSION['swc.site'])) {
	define('swc_site', $_SESSION['swc.site']);
}
// Тема из сессии, надо перенести в swc::__construct()
if (isset($_SESSION['swc.theme'])) {
	define('swc_theme', $_SESSION['swc.theme']);
}

//== check required constants >>
if (!defined('base_path')) {
	define('base_path', $_SERVER['DOCUMENT_ROOT']);
}
if (!defined('swc_base')) {
	define('swc_base', __DIR__);
	//die('Constant "swc_root" could not be defined');
} elseif (swc_base != __DIR__) {
	die('Incorrect swc_base value');
}
if (!defined('web_path')) {
	define('web_path', $_SERVER['DOCUMENT_ROOT']);
}
if (!defined('themes_path')) {
	define('themes_path', base_path . '/themes');
}
if (!defined('swc_data')) {
	define('swc_data', base_path . '/.data');
}
if (!defined('swc_rewrite')) {
	define('swc_rewrite', false);
}
if (!defined('base_url')) {
	if (!swc_rewrite || (swc_rewrite == 'path')) {
		define('base_url', $_SERVER['SCRIPT_FILENAME']);
	} else {
		define('base_url', '/');
	}
}
//<< check required constants ==

require_once(swc_base . '/swc.class.php');
